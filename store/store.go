package store

import (
	uuid "github.com/satori/go.uuid"
)

type Storer interface {
	IsCommitedByID(id uuid.UUID) (bool, error)
	IsCompensatedByID(id uuid.UUID) (bool, error)

	// todo: SaveByID must save status
	SaveByID(id uuid.UUID) error
}
