module gitlab.com/bonch.dev/go-lib/ghor

go 1.15

require (
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
