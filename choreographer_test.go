package gitlab.com/bonch.dev/go-lib/ghor

import (
	"encoding/json"
	"testing"

	uuid "github.com/satori/go.uuid"
)

func TestChoreographer_CreateSaga(t *testing.T) {
	c := NewChoreographer(nil, nil)
	c.CreateSaga(&Saga{
		TaskID:   uuid.NewV4(),
		TaskName: "random_saga",
		data:     json.RawMessage{},
	})
}

func TestChoreographer_Watch(t *testing.T) {
	testScenario := Scenarier(nil)

	c := NewChoreographer(nil)
	c.Register(testScenario)
	c.Watch()
}

/**
hor.RegisterScenario(&TariffUpgradeScenario{})

hor.StartSaga(&TariffUpgradeSaga{
    Receipt: receipt,
    Percon: person
})

todo: как другой вариант создания сценария?
scenario := hor.MakeScenario("some_scenario_name")
scenario.
    AddStep(&SomeFirstStep{}).
    Required()

scenario.
    AddStep(&SomeSecondStep{}).
    Needs(&SomeFirstStep{}).
    Required()

scenario.
    AddStep(&SomeParallelSecondStep{}).
    Needs(&SomeFirstStep{})

scenario.
    AddStep(&SomeThirdParallelStep{}).
    Needs(&SomeFirstStep{}).
    Required()

scenario.
    AddStep(&SomeFourthStep{}).
    Needs(&SomeThirdParallelStep{}, &SomeSecondStep{}).
    Required()

hor.RegisterStep(&SomeFirstStep{})
hor.RegisterStep(&SomeFourthStep{})

 */
