package gitlab.com/bonch.dev/go-lib/ghor

type Scenarier interface {
	Name() string

	InitialStepName() string
	FinalStepName() string
}
