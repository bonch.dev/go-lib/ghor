package transport

import (
	"regexp"

	"github.com/pkg/errors"

	uuid "github.com/satori/go.uuid"
)

var (
	ErrWrongValue = errors.New("wrong value")
)

type MessageID string

func (mid MessageID) IsValid() bool {
	if _, err := mid.ParseID(); err != nil {
		return false
	}

	if _, err := mid.ParseName(); err != nil {
		return false
	}

	if _, err := mid.ParseType(); err != nil {
		return false
	}

	return true
}

func (mid MessageID) parse() (r *regexp.Regexp, parsed []string) {
	r = regexp.MustCompile(`(?P<type>^.*)\.(?P<name>^.*)\.(?P<id>.*)$`)
	parsed = r.FindStringSubmatch(string(mid))

	return
}

func (mid MessageID) ParseID() (uuid.UUID, error) {
	r, parsed := mid.parse()

	if len(parsed) < r.SubexpIndex("id") {
		return uuid.Nil, errors.Wrap(ErrWrongValue, "hasn't been parsed")
	}

	return uuid.FromString(parsed[r.SubexpIndex("id")])
}

func (mid MessageID) MustParseID() uuid.UUID {
	id, err := mid.ParseID()

	if err != nil {
		panic(err.Error())
	}

	return id
}

func (mid MessageID) ParseName() (string, error) {
	r, parsed := mid.parse()

	if len(parsed) < r.SubexpIndex("name") {
		return "", errors.Wrap(ErrWrongValue, "name hasn't been parsed")
	}

	name := parsed[r.SubexpIndex("name")]
	if name == "" {
		return "", errors.Wrap(ErrWrongValue, "name not setted")
	}

	return name, nil
}

func (mid MessageID) MustParseName() string {
	name, err := mid.ParseName()

	if err != nil {
		panic(err.Error())
	}

	return name
}

func (mid MessageID) ParseType() (string, error) {
	r, parsed := mid.parse()

	if len(parsed) < r.SubexpIndex("type") {
		return "", errors.Wrap(ErrWrongValue, "type hasn't been parsed")
	}

	name := parsed[r.SubexpIndex("type")]
	if name == "" {
		return "", errors.Wrap(ErrWrongValue, "type not setted")
	}

	return name, nil
}

func (mid MessageID) MustParseType() string {
	name, err := mid.ParseType()

	if err != nil {
		panic(err.Error())
	}

	return name
}
