package gitlab.com/bonch.dev/go-lib/ghor

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

type BaseSaga struct {
	ID        uuid.UUID     `json:"id"`
	Name 	  string        `json:"name"`
	CreatedAt time.Time     `json:"created_at"`
	Duration  time.Duration `json:"duration"`
}

func (bs BaseSaga) IsAlive() bool {
	if bs.Duration > 0 {
		return time.Now().Before(bs.CreatedAt.Add(bs.Duration))
	}
	
	return true
}


