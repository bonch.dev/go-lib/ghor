package gitlab.com/bonch.dev/go-lib/ghor

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/bonch.dev/go-lib/ghor/store"
	"gitlab.com/bonch.dev/go-lib/ghor/transport"
)

func NewChoreographer(transport transport.Transporter, store store.Storer) *Choreographer {
	return &Choreographer{
		Transport: transport,
		Store:     store,
	}
}

// todo: Choreographer каким образом должен отображать/хранить Scenariers/Steps?
type Choreographer struct {
	Scanariers []Scenarier
	Steps      []Step

	Transport  transport.Transporter
	Store      store.Storer
	Logger     log.Logger
}

// todo: Чем отличаются сценарии от шагов?
func (c *Choreographer) RegisterScenariers(scenariers ...Scenarier) error {
	for _, scenarier := range scenariers {
		c.Scanariers = append(c.Scanariers, scenarier)

		if err := c.Transport.RegisterScenarier(scenarier.Name()); err != nil {
			return err
		}
	}

	return nil
}

// todo: Как шаги должны регистрироваться и имплементироваться?
func (c *Choreographer) RegisterSteps(steps ...Step) error {
	for _, step := range steps {
		c.Steps = append(c.Steps, step)

		if err := c.Transport.RegisterStep(step.Name(), step.ParentName()); err != nil {
			return err
		}
	}

	return nil
}

func (c *Choreographer) Listen(ctx context.Context) error {
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case saga := <-c.Transport.ListenSaga():
			go c.workSaga(saga)
		case step := <-c.Transport.ListenStep():
			go c.workStep(step)
		}
	}
}

func (c *Choreographer) workStep(message transport.StepMessage) {
	step, err := c.findRelatedStep(message)
	if err != nil {
		c.Logger.Println(fmt.Errorf("[ERR]: %w", err).Error())

		return
	}

	var worker Worker
	switch message.Type {
	case transport.CommittedType:
		e, err := c.Store.IsCommitedByID(message.SagaID)
		if err != nil {
			c.Logger.Println(fmt.Errorf("[ERR]: %w", err).Error())

			return
		}

		if e {
			c.Logger.Println("[DEBUG]: tried to execute step, that already persists on this saga")
		}

		worker = step.Executor()
	case transport.FailedType:
		e, err := c.Store.IsCompensatedByID(message.SagaID)
		if err != nil {
			c.Logger.Println(fmt.Errorf("[ERR]: %w", err).Error())

			return
		}

		if e {
			c.Logger.Println("[DEBUG]: tried to compensate step, that already persists on this saga")
		}

		worker = step.Compensator()
	}

	// todo: не должно происходить. Необходимо просто исключить такую радость
	if worker == nil {
		return
	}

	// todo: необходимо подобрать нужный контекст для воркеров
	if err := worker(context.Background(), message.Payload); err != nil {
		// todo 2: теоретически, это нужно вынести на транспортный уровень?
		// c.Transport.Fail(??message??)
		// todo 3: а если это Compensator?
		// fixme: недостаточно просто бросить лог. Тот самый момент, когда время бросить ошибку
		c.Logger.Println(fmt.Errorf("[ERR]: %w", err).Error())

		return
	}

	c.Transport.CommitStep(step)
}

func (c *Choreographer) findRelatedStep(message transport.StepMessage) (Step, error) {
	return nil, nil
}

func (c *Choreographer) workSaga(saga transport.SagaMessage) {
	c.Transport.CommitSaga(s)
}

// todo: Как? Что необходимо передавать в сторону этой функции?
func (c *Choreographer) CallScenarier(scenarier Scenarier) {
	// todo 2: Оно должно обращаться к Transport Layer, для того, чтобы отправить задачу, а не сразу назначить на исполнение
	// Задача на исполнение уходит лишь тогда, когда ее принимает Listen.
	// todo 3: Необходимо Listen заставить прнимать сценарии/саги, и отправлять их на исполнение в другом варианте.
	// todo 4: Имеет ли смысл, отправлять в Transport задачи именно отдельным типом?
}

func (c *Choreographer) IsFinalForScenarier(step Step, scenarier Scenarier) bool {
	return scenarier.FinalStepName() == step.Name()
}
