package gitlab.com/bonch.dev/go-lib/ghor

import (
	"context"
	"encoding/json"
)

type Worker func(ctx context.Context, payload json.RawMessage) error

type Step interface {
	Name() string
	ParentName() string

	IsRequired() bool

	Executor() Worker
	Compensator() Worker
}
