package transport

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
)

func TestSagaID_Parse(t *testing.T) {
	t.Run("Valid", func(t *testing.T) {
		sagaID := MessageID("my_saga_created.0e548061-1026-46d3-91c2-0bcd1fefd2a4")

		assert.Equal(t, "my_saga_created", sagaID.MustParseName())
		assert.Equal(t, uuid.FromStringOrNil("0e548061-1026-46d3-91c2-0bcd1fefd2a4"), sagaID.MustParseID())
		assert.Equal(t, true, sagaID.IsValid())
	})

	t.Run("MustFail_NoUUID", func(t *testing.T) {
		sagaID := MessageID("failer")

		assert.Equal(t, false, sagaID.IsValid())
	})

	t.Run("MustFail_WrongUUID", func(t *testing.T) {
		sagaID := MessageID("failer.id")

		assert.Equal(t, false, sagaID.IsValid())
	})

	t.Run("MustFail_WrongUUID2", func(t *testing.T) {
		sagaID := MessageID("failer.id0e548061-1026-46d3-91c2-0bcd1fefd2a4")

		assert.Equal(t, false, sagaID.IsValid())
	})

	t.Run("MustFail_NoName", func(t *testing.T) {
		sagaID := MessageID(".0e548061-1026-46d3-91c2-0bcd1fefd2a4")

		assert.Equal(t, false, sagaID.IsValid())
	})
}
