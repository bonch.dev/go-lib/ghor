package transport

type Transporter interface {
	// todo: how does register must work
	RegisterScenarier(name string) error
	RegisterStep(name, parentName string) error

	// todo: ???
	Listen() <-chan Message
	Send(message Message) error

	ListenSaga() <-chan SagaMessage
	CommitSaga(message SagaMessage) error
	FailSaga() error

	ListenStep() <-chan StepMessage
	CommitStep(message StepMessage) error
	FailStep() error
}
