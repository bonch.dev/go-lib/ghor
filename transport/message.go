package transport

import (
	"encoding/json"

	uuid "github.com/satori/go.uuid"
)

type Type string

const (
	CreatedType  Type = "created"
	CommittedType Type = "committed"
	FailedType   Type = "failed"
	// todo: another types
)

// todo: Message struct?
type Message struct {
	ID       uuid.UUID `json:"id"`
	SagaName string `json:"saga_name"`
	SagaID   uuid.UUID `json:"saga_id"`
	StepName string `json:"step_name"`

	ID   	 MessageID    `json:"saga_id"`

	Step 	string `json:"step"`

	Payload json.RawMessage `json:"payload"`
}

type SagaMessage struct {
	Message

	FirstStepName string `json:"first_step_name"`
}

type StepMessage Message

